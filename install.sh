#!/bin/bash -e

cd /tmp

#Configure repos:
echo "deb https://deb.debian.org/debian/ buster main contrib non-free
deb https://deb.debian.org/debian/ buster-updates main contrib non-free
deb https://deb.debian.org/debian-security buster/updates main contrib non-free
deb https://deb.debian.org/debian/ buster-backports main contrib non-free
deb-src https://deb.debian.org/debian/ sid main contrib non-free" > /etc/apt/sources.list

#Install software packages
apt-get -qq update
apt-get -yy upgrade
apt-get -yy dist-upgrade
apt-get -yy install adwaita-qt asunder audacity clamav clamtk curl dpkg-dev ffmpeg filezilla firmware-linux-nonfree freecad gchempaint gcu-bin gimp gnome-boxes gnome-shell-extension-caffeine gnome-shell-extension-pixelsaver gnome-shell-extension-appindicator gnome-subtitles gufw handbrake homebank ibus-libpinyin imagej inkscape keepassxc libvirt-clients mkvtoolnix-gui nextcloud-desktop pdfmod plymouth plymouth-themes pspp puddletag python-rgain qrencode qt5-style-plugins qt5-gtk-platformtheme qt5ct r-base remmina rhythmbox-plugin-alternative-toolbar soundconverter spice-vdagent spice-webdavd ttf-mscorefonts-installer wget xul-ext-ublock-origin

#Install LyX + TeXLive
#apt-get -yy install lyx texlive-full

#Install games
#apt-get -yy install 0ad frogatto gnome-games gnome-mastermind pinball supertux supertuxkart unknown-horizons widelands

#Install backported packages
apt-get -yy install -t buster-backports evolution-ews libreoffice

#Install packages from sid source
apt-src -qq update
apt-src -yy install youtube-dl
apt-src -yy build youtube-dl
dpkg -i youtube-dl_*.deb
rm -rf *

#Enable flatpak and flathub:
apt-get -yy install flatpak gnome-software-plugin-flatpak
flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo

#Install flatpak packages
flatpak -y install flathub org.zotero.Zotero

#Make QT apps use GTK theme
echo "QT_QPA_PLATFORMTHEME=qt5ct" > /etc/environment

#Enable graphical boot screen
plymouth-set-default-theme -R futureprototype
sed -i -r 's/^GRUB_TIMEOUT=[0-9]+$/GRUB_TIMEOUT=0/g' /etc/default/grub
sed -i -e 's/GRUB_CMDLINE_LINUX_DEFAULT="quiet"/GRUB_CMDLINE_LINUX_DEFAULT="quiet splash"/g' /etc/default/grub
sed -i -e 's/#GRUB_TERMINAL=console/GRUB_TERMINAL=console/g' /etc/default/grub
RESOLUTION=$(xdpyinfo | grep dimensions | sed -r 's/^[^0-9]*([0-9]+x[0-9]+).*$/\1/g')
sed -i -r 's/^#GRUB_GFXMODE=[0-9]+x[0-9]+$/GRUB_GFXMODE='$RESOLUTION'/g' /etc/default/grub
update-grub2

apt-get -yy autoremove --purge
apt-get -yy autoclean